// Cube with the default color is grey
global const GLfloat cube_strip[] = {
    // front: pos        normals          col
    -1.0, -1.0,  1.0, -1.0, -1.0, -1.0, 0.5, 0.5, 0.5,
    1.0, -1.0,  1.0,   1.0, -1.0, -1.0, 0.5, 0.5, 0.5,
    1.0,  1.0,  1.0,   1.0,  1.0, -1.0, 0.5, 0.5, 0.5,
    -1.0,  1.0,  1.0, -1.0,  1.0, -1.0, 0.5, 0.5, 0.5,
    // back              normals          col
    -1.0, -1.0, -1.0, -1.0, -1.0, 1.0, 1.0, 0.5, 0.5,
    1.0, -1.0, -1.0,   1.0, -1.0, 1.0, 0.5, 0.5, 0.5,
    1.0,  1.0, -1.0,  -1.0,  1.0, 1.0, 0.5, 0.5, 0.5,
    -1.0,  1.0, -1.0, -1.0,  1.0, 1.0, 0.5, 0.5, 0.5
};

// Declares the Elements Array, where the indexs to be drawn are stored
global GLuint elements [] = {
    // front
    0, 1, 2,
    2, 3, 0,
    // right
    1, 5, 6,
    6, 2, 1,
    // back
    7, 6, 5,
    5, 4, 7,
    // left
    4, 0, 3,
    3, 7, 4,
    // bottom
    4, 5, 1,
    1, 0, 4,
    // top
    3, 2, 6,
    6, 7, 3
};



void CreateCube(Mesh *mesh, int pipelineId)
{
    // Load the buffers
    CreateMesh(mesh, pipelineId, true);
    glBindVertexArray(mesh->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cube_strip), cube_strip, GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);
    
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    
    // normal attrib
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    
    // color attrib
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);
    
    glBindVertexArray(0);
    
    mesh->size = sizeof(elements)/sizeof(GLuint);
    mesh->drawMode = DRAW_MODE_TRIANGLE_ELEMENT;
}

void CreateCube(Mesh *mesh, float *color, int pipelineId)
{
    // Create the Vertex Buffer
    float cube[72];
    int idx = 0;
    while(idx < 72)
    {
        // Position
        cube[idx+0] = cube_strip[idx+0];
        cube[idx+1] = cube_strip[idx+1];
        cube[idx+2] = cube_strip[idx+2];
        // Normals
        cube[idx+3] = cube_strip[idx+3];
        cube[idx+4] = cube_strip[idx+4];
        cube[idx+5] = cube_strip[idx+5];
        // Color
        cube[idx+6] = color[0];
        cube[idx+7] = color[1];
        cube[idx+8] = color[2];
        
        idx += 9;
    }
    
    // Load the buffers
    CreateMesh(mesh, pipelineId, true);
    glBindVertexArray(mesh->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);
    
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    
    // normal attrib
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    
    // Tex Corrds
    // Not used for this cube rn
    
    // color attrib
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(3);
    
    glBindVertexArray(0);
    
    mesh->size = sizeof(elements)/sizeof(GLuint);
    mesh->drawMode = DRAW_MODE_TRIANGLE_ELEMENT;
}

void CreateCubeNoElement(Mesh *mesh, int pipelineId)
{
    float vertices[] = {
        // positions          // normals           // texture coords  // color
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        
        0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f, 0.5f, 0.5f, 0.5f,
        0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f, 0.5f, 0.5f, 0.5f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.5f, 0.5f, 0.5f
    };
    
    // Load the buffers
    CreateMesh(mesh, pipelineId, false);
    glBindVertexArray(mesh->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    
    // normal attrib
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    
    // tex coord attrib
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);
    
    // color attrib
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(8 * sizeof(float)));
    glEnableVertexAttribArray(3);
    
    glBindVertexArray(0);
    
    // 36 vertices
    mesh->size = 36;
    mesh->drawMode = DRAW_MODE_TRIANGLE;
}