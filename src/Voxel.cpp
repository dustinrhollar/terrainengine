#include <Voxel.h>

//
global const GLfloat cube_strip_no_col[] = {
    // front: pos       
    -1.0, -1.0, 1.0,
    1.0, -1.0, 1.0,
    1.0,  1.0, 1.0,
    -1.0,  1.0, 1.0,
    // back            
    -1.0, -1.0, -1.0,
    1.0, -1.0, -1.0,
    1.0,  1.0, -1.0,
    -1.0,  1.0, -1.0,
};

/*
To render we need a static cube dataset

For now, assume all Blocks are visible.
Assume a single chunk.

During chunk creation, create and load the buffers

*/


// Returns < 1.0f if the point is in the volume
//         > 1.0f if the point is outside the volume
float sphere_density(SphereVolume *volume, int x, int y, int z)
{
    return (x*x + y*y + z*z) / volume->radius;
}


void createTriList(TriList *list, int cap)
{
    list->size = 0;
    list->capacity = (cap < 0) ? 0 : cap;
    
    list->list = (glm::vec3*)malloc(sizeof(glm::vec3) * list->capacity);
}

void addTri(TriList *list, glm::vec3 tri)
{
    // Using a lower resize since there should be many resizes
    if (list->capacity + 1 >= list->size)
    {
        list->list = (glm::vec3*)realloc(list->list, sizeof(glm::vec3) * list->capacity * 1.5);
    }
    
    list->list[list->size] = tri;
    ++list->size;
}

void freeTri(TriList *list)
{
    list->list = (glm::vec3*)realloc(list->list, 0);
    list->size = 0;
    list->capacity = 0;
}

// x,y,z are the offsets
void createBlock(Block *block, SphereVolume *volume, int x, int y, int z)
{
    /*
    block->isActive = true;
    
    // Determine volume intersection
    float grid[8];
    
    for (int i = 0; i < 24; i += 3)
    {
        sphere_density(volume, cornerOffsets[i + 0] + x,
                       cornerOffsets[i + 1] + y,
                       cornerOffsets[i + 2] + z);
    }
    
    int cubeindex = 0;
    if (grid[0] < 1.0f) cubeindex |= 1;
    if (grid[1] < 1.0f) cubeindex |= 2;
    if (grid[2] < 1.0f) cubeindex |= 4;
    if (grid[3] < 1.0f) cubeindex |= 8;
    if (grid[4] < 1.0f) cubeindex |= 16;
    if (grid[5] < 1.0f) cubeindex |= 32;
    if (grid[6] < 1.0f) cubeindex |= 64;
    if (grid[7] < 1.0f) cubeindex |= 128;
    
    // Cube is entirely in/out of the surface 
    if (edgeTable[cubeindex] == 0)
    {
        block->isActive = false;
        createTriList(&block->tris, 0);
    }
    
    
    
    
    createTriList(&block->tris, 3);
    */
}

void renderBlock(glm::mat4 projection,glm::mat4 view, unsigned int id)
{
    
}

internal void createBlockMesh(float *mesh, GLuint *ele, int id, int x_offset, int y_offset, int z_offset)
{
    int cube_strip_len = sizeof(cube_strip_no_col) / sizeof(float);
    for (int i = 0; i < cube_strip_len; i += 3)
    {
        mesh[i + 0] = 1 * (cube_strip_no_col[i + 0] + x_offset); // x
        mesh[i + 1] = 1 * (cube_strip_no_col[i + 1] + y_offset); // y
        mesh[i + 2] = 1 * (cube_strip_no_col[i + 2] + z_offset); // z
    }
    
    int len = sizeof(elements)/sizeof(GLuint);
    for (int i = 0; i < len; ++i)
    {
        ele[i] = (8*id) + elements[i];
    }
}

void createChunk(Chunk *chunk, SphereVolume *volume, int chunkSize, int pipelineId)
{
    chunk->chunkSize = chunkSize * chunkSize * chunkSize;
    
    // Create Mesh
    CreateMesh(&chunk->mesh, pipelineId, true);
    
    // NOTE(Dustin): This process needs to happen after surface extraction, so that we
    // know the actuall blocks that are visible, but for now, create a buffer for all
    // blocks. I am referring to Buffer creation. 
    // TODO(Dustin): Account for Chunk ID.
    int totalBufferSize = chunk->chunkSize * sizeof(cube_strip);
    float*blockBuffer = (float*)malloc(totalBufferSize);
    float *block_ptr = blockBuffer;
    int ptr_inc = sizeof(cube_strip_no_col) / sizeof(float);
    
    GLuint *elementBuffer = (GLuint*)malloc(chunk->chunkSize * sizeof(elements));
    GLuint *ele_ptr = elementBuffer;
    int ele_inc = sizeof(elements)/sizeof(GLuint);
    
    // to index a 3D array
    // j + i * width + k * width * height
    int wh = chunkSize * chunkSize; 
    int size = wh * chunkSize;
    
    chunk->blocks = (Block*)malloc(sizeof(Block) * size);
    for (int i = 0; i < chunkSize; ++i)
    {
        int idxi = i * chunkSize;
        for (int j = 0; j < chunkSize; ++j)
        {
            for (int k = 0; k < chunkSize; ++k)
            {
                // Create the Block
                int idx = j + idxi + k * wh;
                createBlock(&chunk->blocks[idx], volume, i, j, k);
                
                // Create the Block Mesh
                createBlockMesh(block_ptr, ele_ptr, idx, i, j, k);
                block_ptr += ptr_inc;
                ele_ptr += ele_inc;
            }
        }
    }
    
    chunk->mesh.size = (sizeof(elements)/sizeof(GLuint)) * chunk->chunkSize;
    
    // Load the buffers
    glBindVertexArray(chunk->mesh.VAO);
    glBindBuffer(GL_ARRAY_BUFFER, chunk->mesh.VBO);
    glBufferData(GL_ARRAY_BUFFER, totalBufferSize, blockBuffer, GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, chunk->mesh.EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, chunk->chunkSize * sizeof(elements), elementBuffer, GL_STATIC_DRAW);
    
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    
    glBindVertexArray(0);
    
    free(blockBuffer);
    free(elementBuffer);
}

void renderChunk(Chunk *chunk, glm::mat4 projection, glm::mat4 view)
{
    Pipeline *pipeline = &GlobalPipelineManager->pipes[chunk->mesh.pipelineId];
    Shader *geometryShader = pipeline->geometryShader;
    
    geometryShader->use();
    geometryShader->setMat4("projection", projection);
    geometryShader->setMat4("view", view);
    
    // bind the VAO
    glBindVertexArray(chunk->mesh.VAO);
    
    glm::mat4 model = glm::mat4(1.0);
    geometryShader->setMat4("model", model);
    
    glDrawElements(GL_TRIANGLES, 
                   chunk->mesh.size, 
                   GL_UNSIGNED_INT, 0);
}

void createVoxel(VoxelWorld *voxel, SphereVolume *v, int num_chunks, int chunk_size)
{
    const char *fragment = "shaders/voxelFragmentShader.fs";
    const char *vertex   = "shaders/voxelVertexShader.vs";
    int pipelineId = createPipeline(true, vertex, fragment);
    
    voxel->num_voxel_chunks = num_chunks;
    voxel->volume = v;
    
    voxel->chunks = (Chunk*)malloc(sizeof(Chunk) * num_chunks);
    for (int i = 0; i < num_chunks; ++i)
    {
        createChunk(&voxel->chunks[i], voxel->volume, chunk_size, pipelineId);
    }
}

void renderVoxel(VoxelWorld *voxel, glm::mat4 projection, glm::mat4 view)
{
    for (int i = 0; i < voxel->num_voxel_chunks; ++i)
    {
        renderChunk(&voxel->chunks[i], projection, view);
    }
}
