
SimpleApp::SimpleApp(GLFWwindow* window, bool hasGUI, int width, int height)
: Application(window, hasGUI, width, height)
{
    //CreateMeshList(&meshList, DEFAULT_MESH_LIST_SIZE);
    //CreateLightList(&lightList, 1);
    
    meshList  = NULL;
    lightList = NULL;
    
    setupWorld();
}

SimpleApp::~SimpleApp() = default;

void SimpleApp::guiWindow()
{
    if (!isWindowVisible)
		return;
    
    ImGui::SetNextWindowSize(ImVec2(200, 200), ImGuiCond_FirstUseEver);
    
	ImGuiWindowFlags window_flags = 0;
	if (!ImGui::Begin("Simple App", nullptr, window_flags)) // No bool flag to omit the close button
	{
		// Early out if the window is collapsed, as an optimization
		ImGui::End();
		return;
	}
    
	ImGui::PushItemWidth(-100); // right alifned, keep 180 pixels for the labels
    
    if (ImGui::CollapsingHeader("Directional Light"))
	{
        // color wheel for diffuse specular and ambient
        if (ImGui::ColorEdit3("Ambient", (float*)&directionalLight.ambient))
        {
        }
        if (ImGui::ColorEdit3("Diffuse", (float*)&directionalLight.diffuse))
        {
        }
        if (ImGui::ColorEdit3("Specular", (float*)&directionalLight.specular))
        {
        }
        
        if (ImGui::CollapsingHeader("Direction"))
        {
            // vector for direction
            DirectionalLight *dl = AS_DIRECTIONAL_LIGHT(&directionalLight);
            if (ImGui::DragFloat("x", &dl->direction[0], 0.01f, -1.0f, 1.0f, "%.2f"))
            {
            }
            if (ImGui::DragFloat("y", &dl->direction[1], 0.01f, -1.0f, 1.0f, "%.2f"))
            {
            }
            if (ImGui::DragFloat("y", &dl->direction[2], 0.01f, -1.0f, 1.0f, "%.2f"))
            {
            }
        }
    }
    
    // For Point lights: Show a list of all Point Lights
    // TODO(Dustin): Button for "Add Point Light"
    // ----> ambient, diffuse, specular color (vec3)
    // ----> position
    // ----> constant, linear, quadratic, Have defaults already?
    if (ImGui::CollapsingHeader("Point Lights"))
    {
        for (int i = 0; i < arrlen(lightList); ++i)
        {
            Light *light = &lightList[i];
            
            if (light->type == LIGHT_TYPE_POINT)
            {
                if (ImGui::TreeNode((void*)(intptr_t)i, "Light %d", i))
                {
                    // color wheel for diffuse specular and ambient
                    // TODO(Dustin): Update the mesh's color
                    if (ImGui::ColorEdit3("Ambient", (float*)&light->ambient))
                    {
                    }
                    if (ImGui::ColorEdit3("Diffuse", (float*)&light->diffuse))
                    {
                    }
                    if (ImGui::ColorEdit3("Specular", (float*)&light->specular))
                    {
                    }
                    
                    PointLight *pl = AS_POINT_LIGHT(light);
                    
                    if (ImGui::CollapsingHeader("Position"))
                    {
                        // vector for direction
                        // TODO(Dustin): Update the meshes position
                        if (ImGui::DragFloat("x", &pl->position[0], 0.1f, -100.0f, 100.0f, "%.2f"))
                        {
                        }
                        if (ImGui::DragFloat("y", &pl->position[1], 0.1f, -100.0f, 100.0f, "%.2f"))
                        {
                        }
                        if (ImGui::DragFloat("y", &pl->position[2], 0.1f, -100.0f, 100.0f, "%.2f"))
                        {
                        }
                    }
                    
                    if (ImGui::DragFloat("Constant", &pl->constant, 0.01f, 0.0f, 1.0f, "%.2f"))
                    {
                    }
                    if (ImGui::DragFloat("Linear", &pl->linear, 0.01f, 0.0f, 1.0f, "%.2f"))
                    {
                    }
                    if (ImGui::DragFloat("Quadratic", &pl->quadratic, 0.01f, 0.0f, 1.0f, "%.2f"))
                    {
                    }
                    
                    ImGui::TreePop();
                }
            }
        }
    }
    
    ImGui::PopItemWidth();
	ImGui::End();
}

void SimpleApp::guiEventHandler()
{
    ImGuiIO const& io = ImGui::GetIO();
    
	if (ImGui::IsKeyPressed(' ', false)) // toggle the GUI window display with space key
	{
		isWindowVisible = !isWindowVisible;
	}
    
    const ImVec2 mousePosition = ImGui::GetMousePos(); // Mouse coordinate window client rect.
	const int x = int(mousePosition.x);
	const int y = int(mousePosition.y);
    
    switch (guiState)
    {
        case GUI_STATE_NONE:
        {
            if (!io.WantCaptureMouse) // Only allow camera interactions to begin when not interacting with the GUI.
            {
                if (ImGui::IsMouseDown(0)) // LMB down event?
        		{
                    guiState = GUI_STATE_ORBIT;
        		}
        		else if (ImGui::IsMouseDown(1)) // RMB down event?
        		{
                    guiState = GUI_STATE_DOLLY;
        		}
        		else if (ImGui::IsMouseDown(2)) // MMB down event?
        		{
                    guiState = GUI_STATE_PAN;
        		}
        		else if (io.MouseWheel != 0.0f) // Mouse wheel zoom.
        		{
                }
            }
        } break;
        
        case GUI_STATE_ORBIT:
        {
            if (ImGui::IsMouseReleased(0)) // LMB released? End of orbit mode.
            {
                guiState = GUI_STATE_NONE;
            }
            else
            {
            }
        } break;
        
        case GUI_STATE_DOLLY:
        {
            if (ImGui::IsMouseReleased(1)) // RMB released? End of dolly mode.
            {
                guiState = GUI_STATE_NONE;
            }
            else
            {
            }
        } break;
        
        case GUI_STATE_PAN:
        {
            if (ImGui::IsMouseReleased(2)) // MMB released? End of pan mode.
            {
                guiState = GUI_STATE_NONE;
            }
            else
            {
            }
        } break;
    }
}

void SimpleApp::setupWorld()
{
    Model mod;
    CreateModel(&mod, "junk");
    
    // Cube pipeline
    const char *vert = "shaders/cube.vs";
    const char *frag = "shaders/cube.fs";
    int pipelineId = createPipeline(true, vert, frag);
    Pipeline *pipeline = GetPipeline(pipelineId);
    //&GlobalPipelineManager->pipes[pipelineId];
    
    // Pipeline for a point light
    const char* light_vert = "shaders/lightCube.vs";
    const char* light_frag = "shaders/lightCube.fs";
    int light_pipeline_id = createPipeline(true, light_vert, light_frag);
    Pipeline *light_pipeline = GetPipeline(light_pipeline_id);
    // &GlobalPipelineManager->pipes[light_pipeline_id];
    
    // TODO(Dustin): Pull this functionality into a file-utils file that
    // loads textures from a file.
    // load and create a material texture
    // -------------------------
    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
    unsigned char *data = stbi_load("images/box_diffuse.png", &width, &height, &nrChannels, 0);
    if (data)
    {
        GLenum format;
        if (nrChannels == 1)
            format = GL_RED;
        else if (nrChannels == 3)
            format = GL_RGB;
        else if (nrChannels == 4)
            format = GL_RGBA;
        
        createTexture(&pipeline->diffuseTexture, width, height,
                      format, format, GL_UNSIGNED_BYTE, (char*)data);
        stbi_image_free(data);
    }
    // Going to treat the albedo map as the specular
    stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
    data = stbi_load("images/box_specular.png", &width, &height, &nrChannels, 0);
    if (data)
    {
        GLenum format;
        if (nrChannels == 1)
            format = GL_RED;
        else if (nrChannels == 3)
            format = GL_RGB;
        else if (nrChannels == 4)
            format = GL_RGBA;
        
        createTexture(&pipeline->albedoTexture, width, height,
                      format, format, GL_UNSIGNED_BYTE, (char*)data);
        stbi_image_free(data);
    }
    else
    {
        printf("Could not load box specular!\n");
    }
    
    pipeline->shininess = 32;
    pipeline->hasDiffuse = true;
    pipeline->hasNormal  = false;
    pipeline->hasAlbedo  = true;
    
    // Activate the Textures
    pipeline->geometryShader->use();
    pipeline->geometryShader->setInt("material.diffuse", 0);
    //pipeline->geometryShader.setInt("material.normal", 1);
    pipeline->geometryShader->setInt("material.specular", 2);
    
    // Floor
    Mesh mesh;
    CreateCubeNoElement(&mesh, pipelineId);
    mesh.proj = glm::perspective(glm::radians(camera.Zoom), (float)windowWidth / (float)windowHeight, 0.1f, 5000.0f);
    mesh.model = glm::scale(mesh.model, glm::vec3(5.0f, .2f, 5.0f));
    //AddMesh(&meshList, &mesh);
    arrput(meshList, mesh);
    
    // Regular Cube
    CreateCubeNoElement(&mesh, pipelineId);
    mesh.proj = glm::perspective(glm::radians(camera.Zoom), (float)windowWidth / (float)windowHeight, 0.1f, 5000.0f);
    mesh.model = glm::translate(mesh.model, glm::vec3(0.0f, 2.0f, 0.0f));
    //AddMesh(&meshList, &mesh);
    arrput(meshList, mesh);
    
    // Directional Light
    CreateDirectionalLight(&directionalLight, glm::vec3(-0.2f, -1.0f, -0.3f), 
                           glm::vec3(0.2f, 0.2f, 0.2f),
                           glm::vec3(0.5f, 0.5f, 0.5f),
                           glm::vec3(1.0f, 1.0f, 1.0f));
    
    // Point light + mesh
    float col[3] = {1.0, 1.0, 1.0};
    CreateCube(&mesh, col, light_pipeline_id);
    mesh.proj  = glm::perspective(glm::radians(camera.Zoom), (float)windowWidth / (float)windowHeight, 0.1f, 5000.0f);
    mesh.model = glm::translate(mesh.model, glm::vec3(0.0f, 3.0f, 0.0f));
    mesh.model = glm::scale(mesh.model, glm::vec3(0.1f, 0.1f, 0.1f));
    //AddMesh(&meshList, &mesh);
    arrput(meshList, mesh);
    
    Light pl = {};
    CreatePointLight(&pl, 
                     glm::vec3(0.05f, 0.05f, 0.05f),
                     glm::vec3(0.8f, 0.8f, 0.8f),
                     glm::vec3(1.0f, 1.0f, 1.0f),
                     glm::vec3(0.0f, 3.0f, 0.0f),
                     1.0f,
                     0.09f,
                     0.032f);
    //AddLightToLightList(&lightList, &pl);
    arrput(lightList, pl);
    
    mesh.proj  = glm::perspective(glm::radians(camera.Zoom), (float)windowWidth / (float)windowHeight, 0.1f, 5000.0f);
    mesh.model = glm::translate(glm::mat4(1.0), glm::vec3(2.0f, 2.0f, 0.0f));
    mesh.model = glm::scale(mesh.model, glm::vec3(0.1f, 0.1f, 0.1f));
    //AddMesh(&meshList, &mesh);
    arrput(meshList, mesh);
    
    CreatePointLight(&pl, 
                     glm::vec3(0.05f, 0.05f, 0.05f),
                     glm::vec3(0.8f, 0.8f, 0.8f),
                     glm::vec3(1.0f, 1.0f, 1.0f),
                     glm::vec3(2.0f, 2.0f, 0.0f),
                     1.0f,
                     0.09f,
                     0.032f);
    //AddLightToLightList(&lightList, &pl);
    arrput(lightList, pl);
    
    mesh.proj  = glm::perspective(glm::radians(camera.Zoom), (float)windowWidth / (float)windowHeight, 0.1f, 5000.0f);
    mesh.model = glm::translate(glm::mat4(1.0), glm::vec3(1.0f, 1.0f, 0.0f));
    mesh.model = glm::scale(mesh.model, glm::vec3(0.1f, 0.1f, 0.1f));
    //AddMesh(&meshList, &mesh);
    arrput(meshList, mesh);
    
    CreatePointLight(&pl, 
                     glm::vec3(0.05f, 0.05f, 0.05f),
                     glm::vec3(0.8f, 0.8f, 0.8f),
                     glm::vec3(1.0f, 1.0f, 1.0f),
                     glm::vec3(1.0f, 1.0f, 0.0f),
                     1.0f,
                     0.09f,
                     0.032f);
    //AddLightToLightList(&lightList, &pl);
    arrput(lightList, pl);
}

void SimpleApp::render()
{
    // Prepare GUI for rendering
    if (hasGui)
    {
        guiNewFrame();
        
        guiWindow();
        
        guiEventHandler();
    }
    
    // Render scene
    for (int i = 0; i < arrlen(meshList); ++i)
    {
        // NOTE(Dustin): I do not like this setup. It does not seem like a good approach
        // to have to pass all lights when rendering a single mesh. Does the camera actually
        // have to be passed?
        RenderMesh(&meshList[i], &directionalLight, lightList, &camera);
    }
    
    // Render the GUI
    if (hasGui)
    {
        guiRender();
    }
}

void SimpleApp::shutdown()
{
    //FreeMeshList(&meshList);
    //FreeLightList(&lightList);
    
    arrfree(meshList);
    arrfree(lightList);
    
    Application::shutdown();
}
