
void CreateMMesh(MMesh *mesh, Vertex *verts, u32 *inds, Texture *texts)
{
    mesh->vertices = verts;
    mesh->indices  = inds;
    mesh->textures = texts;
    
    // Create Render data for the mesh
    glGenVertexArrays(1, &mesh->VAO);
    glGenBuffers(1, &mesh->VBO);
    glGenBuffers(1, &mesh->EBO);
    
    glBindVertexArray(mesh->VAO);
    
    glBindBuffer(GL_ARRAY_BUFFER, mesh->VBO);
    glBufferData(GL_ARRAY_BUFFER, arrlen(verts) * sizeof(Vertex), &verts[0], GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, arrlen(inds) * sizeof(u32), &inds[0], GL_STATIC_DRAW);
    
    // positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    
    // normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
    
    // text coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));
    
    glBindVertexArray(mesh->VAO);
}

void RenderMMesh(MMesh *mesh)
{
    // How to handle Lighting info?
    // Ideally, Meshes are grouped by Shader so that lights only have
    // to bound when a specific pipeline is being rendered
    
    // How to handle textures?
    // LearnOpenGL suggests to use strings, and i say, how about no.
    // The solution I will go with for now is specifying a set amount
    // of allowed textures types. Use the sprinf trick over in Mesh.cpp
}

internal void
loadMaterialTextures(Texture *textures, aiMaterial *mat, aiTextureType aiType)
{
    for (u32 i = 0; i < mat->GetTextureCount(aiType); ++i)
    {
        aiString str;
        mat->GetTexture(aiType, i, &str);
        
        Texture texture;
        texture.width = 0;  // Don't ned width and height. will be removed in future
        texture.height = 0;
        texture.id = 0; // TextureFromFile(str.c_str(), );
        
        switch (aiType)
        {
            case aiTextureType_DIFFUSE:  texture.type = TEXTURE_TYPE_DIFFUSE;  break;
            case aiTextureType_SPECULAR: texture.type = TEXTURE_TYPE_SPECULAR; break;
        }
        
        arrput(textures, texture);
    }
}

internal void
processMesh(MMesh *mesh, aiMesh *aiM, const aiScene *scene)
{
    mesh->vertices = NULL;
    mesh->indices  = NULL;
    mesh->textures = NULL;
    
    for (u32 i = 0; i < aiM->mNumVertices; ++i)
    {
        Vertex vert;
        
        // positions
        vert.position.x = aiM->mVertices[i].x;
        vert.position.y = aiM->mVertices[i].y;
        vert.position.z = aiM->mVertices[i].z;
        
        // normals
        vert.normal.x = aiM->mNormals[i].x;
        vert.normal.y = aiM->mNormals[i].y;
        vert.normal.z = aiM->mNormals[i].z;
        
        // tex coords
        if (aiM->mTextureCoords[0])
        {
            vert.texCoords.x = aiM->mTextureCoords[0][i].x;
            vert.texCoords.y = aiM->mTextureCoords[0][i].y;
        }
        else
            vert.texCoords = glm::vec2(0.0f, 0.0f);
        arrput(mesh->vertices, vert);
    }
    
    // indices
    for (u32 i = 0; i < aiM->mNumFaces; ++i)
    {
        aiFace face = aiM->mFaces[i];
        for (u32 j = 0; j < face.mNumIndices; ++j)
        {
            arrput(mesh->indices, face.mIndices[j]);
        }
    }
    
    // material
    Texture *diffuseMaps  = NULL;
    Texture *specularMaps = NULL;
    if (aiM->mMaterialIndex >= 0)
    {
        aiMaterial *material = scene->mMaterials[aiM->mMaterialIndex];
        loadMaterialTextures(diffuseMaps, material, aiTextureType_DIFFUSE); 
        loadMaterialTextures(specularMaps, material, aiTextureType_SPECULAR);
    }
    
}

internal void 
processNode(Model *model, aiNode *node, const aiScene *scene)
{
    for (u32 i = 0; i < node->mNumMeshes; ++i)
    {
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        
        MMesh m;
        processMesh(&m, mesh, scene);
        arrput(model->meshes, m);
    }
    
    for (u32 i = 0; i < node->mNumChildren; ++i)
    {
        processNode(model, node->mChildren[i], scene);
    }
}

void CreateModel(Model *model, const char *path)
{
    model->meshes = NULL;
    
    
    char *str = "this/is/an/example/directory/with/file.fbx";
    char cpy[256];
    strcpy(cpy, str);
    
    char *tmp = strrchr(cpy, '/') + 1;
    int size = tmp - cpy;
    
    char dir[256];
    strncpy(dir, str, size);
    dir[size] = '\0';
    
    printf("%d\t%s\t%s\n", size, tmp, dir);
    
    
    
    /*
    Assimp::Importer import;
    const aiScene *scene = import.ReadFile(path, aiProcess_Triangulate|aiProcess_FlipUVs);
    
    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        fprintf(stderr, "ERROR::ASSIMP::%s\n", import.GetErrorString());
        return;
    }
    */
    // directory = path.substr(0, path.find_last_of('/'));
    
    
    //processNode(model, scene->mRootNode, scene);
}

void DrawModel(Model *model)
{
    for (int i = 0; i < arrlen(model->meshes); ++i)
    {
        RenderMMesh(&model->meshes[i]);
    }
}