// C lib
// TODO(Dustin): Remove C++ headers in favor of C headers
//------------------------------------------
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdio>
#include <stdlib.h>
#include <stdint.h>

// TODO(Dustin): Maybe move these into a *.a file that is linked
// at compile time?
// External Libs
//------------------------------------------
// GLFW
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// STB
#define STB_IMAGE_IMPLEMENTATION
#define STB_DS_IMPLEMENTATION
#include <stb/stb_image.h>
#include <stb/stb_ds.h>

// GLM
// TODO(Dustin): Remove GLM in favor of a lighter math Library
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

// GLAD
#include "glad.c"

// IMGUI
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

// ASSIMP
//#include <assimp/Importer.hpp>
//#include <assimp/scene.h>
//#include <assimp/postprocess.h>

// Header files
//------------------------------------------
// Configuration file for application.
#include <appconfig.h>

// Camera and Shader utility classes to make life easier
#include <Camera.h>
#include <Shader.h>

// Mesh and Render related files
#include <Lighting.h>
#include <Renderer.h>
#include <Mesh.h>
#include <Model.h>

// Voxel algorithms
// NOTE(Dustin): Just for show. Not really implemented yet.
//#include <MarchingCubes.h>
//#include <Voxel.h>

// Procedural Terrain Algorithms
#include <OpenSimplexNoise.h>
#include <TerrainGenerator.h>
#include <Terrain.h>

// LOD System for Terrain
#include <LODMesh.h>

#include <Application.h>

// Source Files
// NOTE(Dustin): LOD source is currently removed from the program
// Voxel Source is currently removed from the program. Neither of these are
// fully working yet and are not undergoing active development. Once I have
// a full renderer working, I will take another look at these components.
//------------------------------------------
// Renderer Source
#include "Lighting.cpp"
#include "Renderer.cpp"

// Geometry source
#include "Mesh.cpp"
//#include "cube.cpp"
//#include "Model.cpp"

// Voxel and Terrain Gen Source
//#include "Voxel.cpp"
#include "TerrainGenerator.cpp"
#include "Terrain.cpp"

// LOD system for terrain
#include "LODMesh.cpp"

// Applications
#include "Application.cpp"
#include "LODApp.cpp"
#include "TerrainApp.cpp"
//#include "VoxelApp.cpp"
//#include "SimpleApp.cpp"

#include "main.cpp"


