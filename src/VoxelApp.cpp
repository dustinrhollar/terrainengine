
VoxelApp::VoxelApp(GLFWwindow* window, bool hasGUI, int width, int height) :
Application(window, hasGUI, width, height)
{
    SphereVolume *sv = (SphereVolume *)malloc(sizeof(SphereVolume));
    sv->radius = 1.0f;
    sv->center = glm::vec3();
    
    voxelTerrain = (VoxelWorld*)malloc(sizeof(VoxelWorld));
    createVoxel(voxelTerrain, sv, 1, 1);
}

VoxelApp::~VoxelApp() = default;

void VoxelApp::guiWindow()
{
    return;
}

void VoxelApp::guiEventHandler()
{
    return;
}

void VoxelApp::render()        
{
    // configure global opengl state
    // -----------------------------
    //glEnable(GL_DEPTH_TEST);
    
#if WIREFRAME
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
#endif
    
    glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)windowWidth / (float)windowHeight, 0.1f, 5000.0f);
    glm::mat4 view = camera.GetViewMatrix();
    renderVoxel(voxelTerrain, projection, view);
}

void VoxelApp::shutdown()
{
}
