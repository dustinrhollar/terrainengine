#ifndef VOXEL_H
#define VOXEL_H

typedef struct
{
    float radius;
    glm::vec3 center;
} SphereVolume;

typedef struct
{
    int size;
    int capacity;
    glm::vec3 *list;
} TriList;

// a block can be active and have a "type"
typedef struct 
{
    bool isActive;
    TriList tris;
} Block;


typedef struct
{
    int chunkSize;
    Mesh mesh;
    Block *blocks;
} Chunk;

typedef struct
{
    int num_voxel_chunks;
    SphereVolume *volume;
    //int pipelineId;
    Chunk *chunks;
} VoxelWorld;

// Returns < 1.0f if the point is in the volume
//         > 1.0f if the point is outside the volume
float sphere_density(SphereVolume *volume, int x, int y, int z);

void createTriList(TriList *list, int cap);
void addTri(TriList *list, glm::vec3 tri);
void freeTri(TriList *list);

// Not sure if I want these yet
// types: grass, dirst, water, stone, wood, sand

void createBlock(Block *block, SphereVolume *volume);
void renderBlock(glm::mat4 projection, glm::mat4 view, unsigned int id);


// constains some set of Blocks
// hands rendering these blocks for a given chunk

void createChunk(Chunk *chunk, SphereVolume *volume, int chunkSize, int pipelineId);
void renderChunk(Chunk *chunk, glm::mat4 projection, glm::mat4 view);

void createVoxel(VoxelWorld *voxel, SphereVolume *v, int num_chunks, int chunk_size);
void renderVoxel(VoxelWorld *voxel, glm::mat4 projection, glm::mat4 view);

#endif // VOXEL_H