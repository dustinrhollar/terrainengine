#ifndef MODEL_H
#define MODEL_H

typedef struct
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoords;
} Vertex;

typedef struct
{
    u32 VAO, VBO, EBO; // Render data
    Vertex  *vertices; // dyn array
    u32     *indices;  // dyn array
    Texture *textures; // dyn array
} MMesh;

typedef struct
{
    MMesh *meshes; // dyn array
    //char *directory
} Model;

// Determine:
//   How does vertices, indices, and textures get sent to Model?
//   For now, just pass the pointer and see how things go

void CreateMMesh(MMesh *mesh, Vertex *verts, u32 *inds, Texture *texts);
void RenderMMesh(MMesh *mesh);


void CreateModel(Model *model, const char *filepath);
void DrawModel(Model *model);


#endif